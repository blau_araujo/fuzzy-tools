# fuzzy-tools

Ferramentas úteis no terminal com o **fzf** (https://github.com/junegunn/fzf)

## Roadmap:

- [x] Unir as funções em um único script.
- [x] Criar aquivo de configuração do usuário em ~/.config/fuzzy-tools
- [x] Checar dependência (fzf)
- [x] Implementar sistema onde as novas funções entrem como "plugins"
- [x] Redefinir como o usuário faz as configurações de plugins (array, source, outros?)
- [ ] ~~Encontrar um meio simples de tornar a função `file_search` customizável~~ Agora é um plugin.
- [ ] ~~Implementar~~ Criar **plugin** de busca de pacotes Fedora (se possível) https://gitlab.com/blau_araujo/fuzzy-tools/-/issues/1
- [x] Criar plugin de busca de pacotes do Arch Linux
- [ ] Criar plugin de buscas customizadas por arquivos de texto
- [x] Definir como o usuário terá acesso aos plugins padrão
- [ ] Definir quais serão os plugins habilitados por padrão
